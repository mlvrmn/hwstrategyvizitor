﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWStrategyVizitor
{
    public interface IStrategySerialization
    {
        String ExecuteSerialization(Object visiter);
    }
}
