﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace HWStrategyVizitor
{
    public class StrategyXmlSerialization : IStrategySerialization
    {
        public String ExecuteSerialization(Object visiter)
        {
            var type = visiter.GetType();
            var propertiesInfo = type.GetProperties();

            var stringBuilder = new StringBuilder();

            stringBuilder.Append("XMLString : <");
            foreach (var property in propertiesInfo)
            {
                stringBuilder.Append(property.Name + ": ");
                stringBuilder.Append(property.GetValue(visiter).ToString());

                if (property != propertiesInfo.Last())
                    stringBuilder.Append(", ");
            }
            stringBuilder.Append(" >");

            var serializeObj = stringBuilder.ToString();
            return serializeObj;
        }
    }
}
