﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWStrategyVizitor
{
    public abstract class Figure
    {
        public String Name { get; set; }
        public String Color { get; set; }
        public Int32 Area { get; set; }

        public Figure(String name, String color, Int32 area)
        {
            this.Name = name;
            this.Color = color;
            this.Area = area;
        }
    }
}
