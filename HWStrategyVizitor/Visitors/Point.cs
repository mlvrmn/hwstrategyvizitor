﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWStrategyVizitor
{
    public class Point : Figure ,IVisitor
    {
        public String Visit(StrategyJsonSerialization strategyJson)
        {
            String data = strategyJson.ExecuteSerialization(this);
            return data;
        }

        public String Visit(StrategyXmlSerialization strategyXml)
        {
            String data = strategyXml.ExecuteSerialization(this);
            return data;
        }

        public Point(String name, String color, Int32 area)
            : base(name, color, area)
        {

        }
    }
}
