﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWStrategyVizitor
{
    public interface IVisitor
    {
        String Visit(StrategyJsonSerialization strategyJson);
        String Visit(StrategyXmlSerialization strategyXml);
    }
}
