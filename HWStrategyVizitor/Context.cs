﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWStrategyVizitor
{
    public class Context
    {
        IStrategySerialization _strategySerialization;
        IVisitor _visitor;

        private void SetVizitor(String name)
        {
            switch (name)
            {
                case "круг":
                    _visitor = new Circle(name, "красный", 20);
                    break;

                case "точка":
                    _visitor = new Point(name, "черный", 1);
                    break;

                case "квадрат":
                    _visitor = new Square(name, "зеленый", 100);
                    break;
            }
        }

        private void SetStrategy(String serializationType)
        {
            switch (serializationType)
            {
                case "json":
                    _strategySerialization = new StrategyJsonSerialization();
                    break;

                case "xml":
                    _strategySerialization = new StrategyXmlSerialization();
                    break;
            }
        }

        public String SerializeVisitor(String name, String serializationType)
        {
            SetVizitor(name);

            SetStrategy(serializationType);

            String data = String.Empty;

            if (_strategySerialization is StrategyJsonSerialization)
                data = _visitor.Visit((StrategyJsonSerialization)_strategySerialization);

            if (_strategySerialization is StrategyXmlSerialization)
                data = _visitor.Visit((StrategyXmlSerialization)_strategySerialization);

             return data;

        }
    }
}
