﻿// Че будет : 

// нужно сформировать 3 геометрических фигуры : точка круг квадрат 
// нужно сделать 2 стратегии : 1 стратегия сериализации json, другая xml
// нужно сделать 3 визитора. Каждый visit-метод должен вернуть сериализацию указанной фигуры в указанном формате.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWStrategyVizitor
{
   public class Program
    {
        /*При запуске вписывать только варианты фигур и типа сериализации как предлагается :)*/
        static void Main(string[] args)
        {
            Context context = new Context();

            Console.WriteLine("Вас приветствует самопальный \"сериализатор\" ");

            Console.WriteLine("Какую фигуру сериализовать ? (круг, квадрат или точка)");

            String nameVisitor = Console.ReadLine();

            Console.WriteLine("В каком формате сериализовать ? (json, xml)");

            String typeSerialization = Console.ReadLine();

            String data = context.SerializeVisitor(nameVisitor, typeSerialization);

            Console.WriteLine(data);

            Console.ReadKey();
        }
    }
}
